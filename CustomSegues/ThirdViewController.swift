//
//  ThirdViewController.swift
//  CustomSegues
//
//  Created by Александр Исаев on 27.02.15.
//  Copyright (c) 2015 Александр Исаев. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        var swipeGestureRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "showFirstViewController")
        swipeGestureRecognizer.direction = UISwipeGestureRecognizerDirection.Up
        self.view.addGestureRecognizer(swipeGestureRecognizer)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showFirstViewController () {
        self.performSegueWithIdentifier("idSecondSegueUnwind", sender: self)
    }
}
