//
//  SecondViewController.swift
//  CustomSegues
//
//  Created by Александр Исаев on 27.02.15.
//  Copyright (c) 2015 Александр Исаев. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var lblMessage: UILabel!
    var message: NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var swipeGestureRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "showFirstViewController")
        swipeGestureRecognizer.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(swipeGestureRecognizer)
        
        lblMessage.text = message
    }

    func showFirstViewController() {
        self.performSegueWithIdentifier("idFirstSegueUnwind", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "idFirstSegueUnwind" {
            let firstViewController = segue.destinationViewController as ViewController
            firstViewController.lblMessage.text = "You just came from the 2nd VC"
        }
    }

}
